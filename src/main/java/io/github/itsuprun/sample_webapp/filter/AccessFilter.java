package io.github.itsuprun.sample_webapp.filter;

import io.github.itsuprun.sample_webapp.controller.helper.ControllerHelper;
import io.github.itsuprun.sample_webapp.domain.Role;
import io.github.itsuprun.sample_webapp.domain.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class AccessFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        final String requestURI = httpRequest.getRequestURI();
        if (requestURI.contains("adminpanel")) {
            final Role userRole = Optional.ofNullable((User) httpRequest.getSession().getAttribute("user"))
                    .map(User::getUserRole)
                    .orElse(null);
            if (userRole == null) {
                httpResponse.sendRedirect("/login");
            }
            if (userRole == Role.ADMIN) {
                chain.doFilter(httpRequest, httpResponse);
                //httpResponse.sendRedirect("/adminpanel");
            }
            if (userRole == Role.CLIENT) {
                httpRequest.setAttribute("messageFromServer", "Admin room closed for you!");
                httpResponse.sendRedirect("/");
            }
        }
        else {
            chain.doFilter(httpRequest, httpResponse);
        }

    }

    @Override
    public void destroy() {

    }
}
