package io.github.itsuprun.sample_webapp;

import io.github.itsuprun.sample_webapp.controller.AdminPanelCommand;
import io.github.itsuprun.sample_webapp.controller.BookingApplicationCommand;
import io.github.itsuprun.sample_webapp.controller.Command;
import io.github.itsuprun.sample_webapp.controller.LoginCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class ServletHelper {

    private Map<String, Command> commands = new HashMap<>();

    public ServletHelper() {
        commands.put("login", new LoginCommand());
        commands.put("application", new BookingApplicationCommand());
        commands.put("adminpanel", new AdminPanelCommand());
    }

    public String executeCommandByPath(String path, HttpServletRequest request) {
        String pageView = commands.get(path).commandAction(request);
        return pageView;
    }

}
