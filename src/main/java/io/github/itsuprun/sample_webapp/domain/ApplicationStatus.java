package io.github.itsuprun.sample_webapp.domain;

public enum ApplicationStatus {
    NEW, PROCESSED
}
