package io.github.itsuprun.sample_webapp.domain;

import java.time.LocalDate;

public class BookingApplication extends BaseEntity {

    // Variables

    private Integer numberOfBeds;
    private RoomClass roomClass;
    private LocalDate bookingDateStart;
    private LocalDate bookingDateEnd;
    private User user;
    private ApplicationStatus applicationStatus;

    // Constructors

    public BookingApplication() {
    }

    public BookingApplication(Integer numberOfBeds,
                              RoomClass roomClass,
                              LocalDate bookingDateStart,
                              LocalDate bookingDateEnd,
                              User user,
                              ApplicationStatus status) {
        this.numberOfBeds = numberOfBeds;
        this.roomClass = roomClass;
        this.bookingDateStart = bookingDateStart;
        this.bookingDateEnd = bookingDateEnd;
        this.user = user;
        this.applicationStatus = status;
    }

    public Integer getNumberOfBeds() {
        return numberOfBeds;
    }

    public void setNumberOfBeds(Integer numberOfBeds) {
        this.numberOfBeds = numberOfBeds;
    }

    public RoomClass getRoomClass() {
        return roomClass;
    }

    public void setRoomClass(RoomClass roomClass) {
        this.roomClass = roomClass;
    }

    public LocalDate getBookingDateStart() {
        return bookingDateStart;
    }

    public void setBookingDateStart(LocalDate bookingDateStart) {
        this.bookingDateStart = bookingDateStart;
    }

    public LocalDate getBookingDateEnd() {
        return bookingDateEnd;
    }

    public void setBookingDateEnd(LocalDate bookingDateEnd) {
        this.bookingDateEnd = bookingDateEnd;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ApplicationStatus getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(ApplicationStatus applicationStatus) {
        this.applicationStatus = applicationStatus;
    }
}
