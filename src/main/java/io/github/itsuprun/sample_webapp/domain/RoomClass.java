package io.github.itsuprun.sample_webapp.domain;

public enum RoomClass {
    ECONOM, SEMILUX, LUX
}
