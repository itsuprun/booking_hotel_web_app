package io.github.itsuprun.sample_webapp.service;

import io.github.itsuprun.sample_webapp.database.BookingApplicationDao;
import io.github.itsuprun.sample_webapp.database.DaoFactory;
import io.github.itsuprun.sample_webapp.domain.ApplicationStatus;
import io.github.itsuprun.sample_webapp.domain.BookingApplication;
import io.github.itsuprun.sample_webapp.domain.RoomClass;
import io.github.itsuprun.sample_webapp.dto.BookingApplicationDto;

import java.time.LocalDate;
import java.util.List;

public class BookingApplicationService {

    private final DaoFactory daoFactory = DaoFactory.getInstance();

    public void createBookingApplication(BookingApplicationDto bookingApplicationDto) {
        final BookingApplication bookingApplication = new BookingApplication(
                Integer.parseInt(bookingApplicationDto.getNumberOfBeds()),
                RoomClass.valueOf(bookingApplicationDto.getRoomClass()),
                LocalDate.parse(bookingApplicationDto.getBookingStartDate()),
                LocalDate.parse(bookingApplicationDto.getBookingEndDate()),
                bookingApplicationDto.getUser(),
                ApplicationStatus.NEW
        );
        try (final BookingApplicationDao bookingApplicationDao = daoFactory.createBookingApplicationDao()) {
            bookingApplicationDao.create(bookingApplication);
        }
    }

    public List<BookingApplication> getAllActiveApplications() {
        try (final BookingApplicationDao bookingApplicationDao = daoFactory.createBookingApplicationDao()) {
            return bookingApplicationDao.findByStatus(ApplicationStatus.NEW);
        }
    }
}
