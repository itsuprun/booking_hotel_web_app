package io.github.itsuprun.sample_webapp.service;

import io.github.itsuprun.sample_webapp.database.DaoFactory;
import io.github.itsuprun.sample_webapp.database.UserDao;
import io.github.itsuprun.sample_webapp.domain.User;
import io.github.itsuprun.sample_webapp.dto.LoginDto;
import io.github.itsuprun.sample_webapp.dto.RegisterDto;

import java.util.List;

public class UserService {


    private final DaoFactory daoFactory = DaoFactory.getInstance();

    public User checkLoginForCorrectPassword(LoginDto loginDto) {
        try (final UserDao userDao =  daoFactory.createUserDao()) {
            final User foundUser = userDao.findByEmail(loginDto.getEmail());
            if (foundUser != null && loginDto.getPassword().equals(foundUser.getPassword()))
                return foundUser;
        }
        return null;
    }

    public List<User> getAllUsers() {
        try (UserDao userDao = daoFactory.createUserDao()) {
            return userDao.findAll();
        }

    }

    public boolean registerUser(RegisterDto registrationInfo) {
        try (UserDao userDao = daoFactory.createUserDao()) {
            userDao.create(new User(registrationInfo.getFirstName(),
                    registrationInfo.getLastName(),
                    registrationInfo.getEmail(),
                    registrationInfo.getPassword()));
        } catch (RuntimeException ex) {
            return false; // if sql-exception is thrown
        }
        return true;
    }

}
