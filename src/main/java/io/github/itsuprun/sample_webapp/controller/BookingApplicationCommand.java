package io.github.itsuprun.sample_webapp.controller;

import io.github.itsuprun.sample_webapp.controller.helper.ControllerHelper;
import io.github.itsuprun.sample_webapp.domain.RoomClass;
import io.github.itsuprun.sample_webapp.domain.User;
import io.github.itsuprun.sample_webapp.dto.BookingApplicationDto;
import io.github.itsuprun.sample_webapp.service.BookingApplicationService;

import javax.servlet.http.HttpServletRequest;

public class BookingApplicationCommand implements Command {

    private final BookingApplicationService bookingApplicationService;
    private static final String CONTROLLER_PAGE = "client/bookingApplication.jsp";

    public BookingApplicationCommand() {
        this.bookingApplicationService = new BookingApplicationService();
    }

    @Override
    public String commandAction(HttpServletRequest request) {
        RoomClass[] values = RoomClass.values();
        request.setAttribute("roomClassesList", values);

        if (ControllerHelper.ifAllEmpty(request, new String[]{"roomClass"}))
            return CONTROLLER_PAGE;

        String numberOfBeds = request.getParameter("numberOfBeds");
        String roomClass = request.getParameter("roomClass");
        String bookingStartDate = request.getParameter("bookingDateStart");
        String bookingEndDate = request.getParameter("bookingDateEnd");
        User user = (User) request.getSession().getAttribute("user");

        BookingApplicationDto bookingApplicationDto = new BookingApplicationDto(numberOfBeds,
                roomClass, bookingStartDate,bookingEndDate, user);

        bookingApplicationService.createBookingApplication(bookingApplicationDto);
        return ControllerHelper.returnWithMessage("Your application is saved!", request, "index.jsp");
    }
}
