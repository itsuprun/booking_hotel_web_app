package io.github.itsuprun.sample_webapp.controller;

import io.github.itsuprun.sample_webapp.controller.helper.ControllerHelper;
import io.github.itsuprun.sample_webapp.dto.RegisterDto;
import io.github.itsuprun.sample_webapp.service.UserService;

import javax.servlet.http.HttpServletRequest;

public class RegisterCommand implements Command {

    private static final String REGISTER_PAGE = "register.jsp";
    private final UserService userService;

    public RegisterCommand() {
        this.userService = new UserService();
    }

    @Override
    public String commandAction(HttpServletRequest request) {
        String[] fieldNames = {"first_name", "last_name", "email", "password"};

        if (ControllerHelper.ifAllEmpty(request, fieldNames))
            return REGISTER_PAGE;

        if (ControllerHelper.ifAnyEmpty(request, fieldNames))
            return ControllerHelper.returnWithMessage("All fields must be valid", request, REGISTER_PAGE);

        RegisterDto registerDto = new RegisterDto();
        registerDto.setFirstName(request.getParameter("first_name"));
        registerDto.setLastName(request.getParameter("last_name"));
        registerDto.setEmail(request.getParameter("email"));
        registerDto.setPassword(request.getParameter("password"));

        if(!userService.registerUser(registerDto))
             return ControllerHelper.returnWithMessage("Looks like someone with this email is exists",
                     request,
                     REGISTER_PAGE);

        return "index.jsp";

    }
}
