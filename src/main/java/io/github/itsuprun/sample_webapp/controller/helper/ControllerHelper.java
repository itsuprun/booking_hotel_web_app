package io.github.itsuprun.sample_webapp.controller.helper;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Stream;

public class ControllerHelper {
    public static String returnWithMessage(String message, HttpServletRequest request, String returnPage) {
        request.setAttribute("messageFromServer", message);
        return returnPage;
    }

    public static boolean ifAllEmpty(HttpServletRequest request, String[] fieldNames) {
        return Stream.of(fieldNames)
                .map(request::getParameter)
                .allMatch(ControllerHelper::checkForEmpty);
    }

    public static boolean ifAnyEmpty(HttpServletRequest request, String[] fieldNames) {
        return Stream.of(fieldNames)
                .map(request::getParameter)
                .anyMatch(ControllerHelper::checkForEmpty);
    }

    public static boolean checkForEmpty(String field) {
        if (field == null) return true;
        else if ("".equals(field.trim())) return true;
        else return false;
    }
}
