package io.github.itsuprun.sample_webapp.database;

import io.github.itsuprun.sample_webapp.domain.Reservations;

public interface ReservationsDao extends AbstractDao<Reservations, Integer>{

}
