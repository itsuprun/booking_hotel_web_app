package io.github.itsuprun.sample_webapp.database.mappers;

import io.github.itsuprun.sample_webapp.domain.Role;
import io.github.itsuprun.sample_webapp.domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements ObjectMapper<User> {

    @Override
    public User extract(ResultSet resultSet) throws SQLException {
        User user = new User(resultSet.getInt("id"),
                resultSet.getString("firstName"),
                resultSet.getString("lastName"),
                resultSet.getString("email"),
                resultSet.getString("password"));
        user.setUserRole(Role.valueOf(resultSet.getString("userRole")));
        return user;

    }
}
