package io.github.itsuprun.sample_webapp.database.mappers;

import io.github.itsuprun.sample_webapp.domain.ApplicationStatus;
import io.github.itsuprun.sample_webapp.domain.BookingApplication;
import io.github.itsuprun.sample_webapp.domain.RoomClass;
import io.github.itsuprun.sample_webapp.domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class BookingApplicationMapper implements ObjectMapper<BookingApplication>{

    @Override
    public BookingApplication extract(ResultSet resultSet) throws SQLException {
        final User extractedUser = new UserMapper().extract(resultSet);
        BookingApplication bookingApplication = new BookingApplication(
                resultSet.getInt("number_of_beds"),
                RoomClass.valueOf(resultSet.getString("room_class")),
                resultSet.getDate("booking_date_start").toLocalDate(),
                resultSet.getDate("booking_date_end").toLocalDate(),
                extractedUser,
                ApplicationStatus.valueOf(resultSet.getString("application_status"))
        );
        bookingApplication.setId(resultSet.getInt("id"));
        return bookingApplication;
    }
}
