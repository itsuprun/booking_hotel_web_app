package io.github.itsuprun.sample_webapp.database.rel_impl;

import io.github.itsuprun.sample_webapp.database.UserDao;
import io.github.itsuprun.sample_webapp.database.mappers.UserMapper;
import io.github.itsuprun.sample_webapp.domain.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCUserDao implements UserDao {

    private final Connection connection;

    public JDBCUserDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public User findById(Integer id) {
        String preparedStatementString = "SELECT * FROM user WHERE user.id = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(preparedStatementString)) {
            preparedStatement.setInt(1, id);
            final ResultSet resultSet = preparedStatement.executeQuery();

            final UserMapper userMapper = new UserMapper();
            User user = null;
            while (resultSet.next()) {
                user = userMapper.extract(resultSet);
            }

            return user;
        } catch (SQLException throwables) {
            throw new RuntimeException("Unable to find user by Id");
        }
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        String preparedStatementString = "select * from user;";

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(preparedStatementString);
            UserMapper userMapper = new UserMapper();

            while(resultSet.next()) {
                users.add(userMapper.extract(resultSet));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return users;
    }

    @Override
    public void create(User obj) {
        String preparedCreateStatementString = "INSERT INTO user (firstName, lastName, email, password) VALUES " +
                "(?,?,?,?);";
        try (PreparedStatement preparedStatement = connection.prepareStatement(preparedCreateStatementString)) {
            preparedStatement.setString(1, obj.getFirstName());
            preparedStatement.setString(2, obj.getLastName());
            preparedStatement.setString(3, obj.getEmail());
            preparedStatement.setString(4, obj.getPassword());

            if (preparedStatement.executeUpdate() == 0) {
                throw new RuntimeException("Nothing was added!");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public void update(User obj) {

    }

    @Override
    public void deleteById(Integer id) {

    }

    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public User findByEmail(String email) {
        String preparedStatementString = "SELECT * FROM user WHERE user.email = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(preparedStatementString)) {
            preparedStatement.setString(1, email);
            final ResultSet resultSet = preparedStatement.executeQuery();

            final UserMapper userMapper = new UserMapper();
            User user = null;
            while (resultSet.next()) {
                user = userMapper.extract(resultSet);
            }

            return user;
        } catch (SQLException throwables) {
            throw new RuntimeException("Unable to find user by Id");
        }
    }
}
