package io.github.itsuprun.sample_webapp.database;

import io.github.itsuprun.sample_webapp.domain.User;

public interface UserDao extends AbstractDao<User, Integer> {
    User findByEmail(String email);
}
