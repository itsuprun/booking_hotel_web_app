<%--
  Created by IntelliJ IDEA.
  User: onetwostory
  Date: 3/19/21
  Time: 8:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
    <h2>Register</h2>
    <form action="register" method="post">
        <h3>First name:</h3>
        <input type="text" name="first_name" placeholder="Ivan" required>
        <h3>Last name:</h3>
        <input type="text" name="last_name" placeholder="Ivanov" required>
        <br>
        <h3>Email:</h3>
        <input type="text" name="email" placeholder="ivan@ass.com" required>
        <h3>Password:</h3>
        <input type="password" name="password" placeholdes="password" required>
        <br><br>
        <input type="submit" value="Register">
        ${messageFromServer}
    </form>
</body>
</html>
