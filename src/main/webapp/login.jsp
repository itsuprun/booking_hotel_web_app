
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
    <h1>Login</h1>
    <form action="login" method="post">
        <h3>Input your email:</h3>
        <input name="email" type="email" placeholder="Email" required>
        <h3>Input your password:</h3>
        <input name="password" type="password" placeholder="Password" required>
        <br><br>
        <input type="submit" value = 'Login'>
        <h4><a href = '/register'>I'm unregistered user!</a></h4>
        ${messageFromServer}
    </form>
</body>
</html>
