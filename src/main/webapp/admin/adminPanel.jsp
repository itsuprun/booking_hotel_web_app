<%--
  Created by IntelliJ IDEA.
  User: itsuprun
  Date: 3/10/21
  Time: 8:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Admin panel</title>
</head>
<body>
<h3>Applications:</h3>
<table border="1px solid" cellspacing="0" cellpadding="5px">
    <thead>
        <tr>
            <td>id</td>
            <td>number of beds</td>
            <td>room class</td>
            <td>date start</td>
            <td>date end</td>
            <td>user id</td>
            <td>user name</td>
            <td>user surname</td>
            <td>user email</td>
            <td>status</td>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="application" items="${boookingApplication}">
            <tr>
                <td>${application.id}</td>
                <td>${application.numberOfBeds}</td>
                <td>${application.roomClass}</td>
                <td>${application.bookingDateStart}</td>
                <td>${application.bookingDateEnd}</td>
                <td>${application.user.id}</td>
                <td>${application.user.firstName}</td>
                <td>${application.user.lastName}</td>
                <td>${application.user.email}</td>
                <td>${application.applicationStatus.toString()}</td>
            </tr>
        </c:forEach>
    </tbody>
</table>
</body>
</html>
